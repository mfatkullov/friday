import React from 'react';
import logo from "../logo.svg";
import App from "../App";
import Message from "./Message";

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            weekday: new Date().toLocaleString('en-en', {weekday: 'long'})
        };
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            weekday: new Date().toLocaleString('en-en', {weekday: 'long'}),
        });
    }

    render() {
        const classes = `App-header ${this.state.weekday}`
        return (
            <header className={classes}>
                <img src={logo} className="App-logo" alt="logo" />
                <Message weekday={this.state.weekday}/>
            </header>
        )
    }
};

export default Header;