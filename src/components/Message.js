import React from 'react';

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: Capitalize(new Date().toLocaleString(window.navigator.language, {weekday: 'long'}))
        };
    }

    render() {
        switch(this.state.weekday) {
            case 'Friday':
                return <h1>{this.state.message}</h1>;
            default:
                return <h1>{this.props.weekday}</h1>;
        }
    }
};

function Capitalize(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export default Message;